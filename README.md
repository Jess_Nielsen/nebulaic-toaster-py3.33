Hello Bitbucketeer
=====

Based on [Nebulaic-Toaster](https://bitbucket.org/chromebookbob/nebulaic-toaster) by Chromebookbob

It is a forked repo made for Python 3.33.

The old master branch has been fast-forwarded to reflect the new Python 3.33 version. 
This has deprecated the old OOP-Nebulaic-Toaster branch, which has been removed.

The branches are now:

Branches:
=========

Master
-------
Contains the working Python 3.33 version of the original Nebulaic-Toaster (link above)
All commits to this branch must have sufficient testing and be thoroughly documented.

dev-Branch
-----------
Contains the development branch. This a not even an alpha, clone at you own risk.
Commits to this branch will not necessarily have sufficient testing or be thoroughly documented.

Downloads:
==========

Currently the downloads are switched off. Until the need arises, those interested will have to clone the repo.